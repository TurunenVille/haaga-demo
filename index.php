<!DOCTYPE html>
<?php
$answer = "";
if(isset($_POST['appbox']) && !empty($_POST['appbox'])) {
  if ($_POST['appbox'] == "Hello") {
    $answer = "Hello Haaga!";
  }
  elseif ($_POST['appbox'] == "hello") {
    $answer = "hello haaga!";
  }
  elseif ($_POST['appbox'] == "HELLO") {
    $answer = "HELLO HAAGA!!!";
  }
  else {
    $answer = "";
  }
}
?>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Hello Demo</title>
  <meta name="author" content="Ville Turunen">
  <meta name="description" content="Hello... ?">
  <meta name="keywords" content="hello">
  <link rel="stylesheet" href="stylesheet.css" type="text/css">

  </head>
  <body>
    <div class="container">
      <div id="input-field">
        <form name="form" action="<?php $_PHP_SELF?>" method="post">
          <input type = "Text" name = "appbox" maxlength="10" size="10">
        </form>
      </div>
      <div id="answer-field">
        <?php echo $answer ?>
      </div>
    </div>
  </body>
</html>